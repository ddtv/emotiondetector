//
//  ViewController.m
//  EmotionGraph
//
//  Created by Doddabela, Supreeth A (US - Bengaluru) on 2/8/17.
//  Copyright © 2017 Deloitte. All rights reserved.
//

#import "ViewController.h"
#import "CameraFeedViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cameraFeedButtonTapped:(id)sender {
    
    CameraFeedViewController *cameraFeedViewController = [[CameraFeedViewController alloc] initWithNibName:@"CameraFeedViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:cameraFeedViewController animated:YES];
}
@end
