//
//  AppDelegate.h
//  EmotionGraph
//
//  Created by Doddabela, Supreeth A (US - Bengaluru) on 2/8/17.
//  Copyright © 2017 Deloitte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

