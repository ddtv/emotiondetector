//
//  CameraFeedViewController.m
//  EmotionGraph
//
//  Created by Doddabela, Supreeth A (US - Bengaluru) on 2/8/17.
//  Copyright © 2017 Deloitte. All rights reserved.
//

#import "CameraFeedViewController.h"

static const NSUInteger kMaxDataPoints = 52;
static NSString *const kPlotIdentifier = @"Smile";
static NSString *const kPlotIdentifierForAnger = @"Anger";
static NSString *const kPlotIdentifierForAverage = @"Surprise";
static const double kFrameRate = 5.0;  // frames per second
static const double kAlpha     = 0.25; // smoothing constant



@interface CameraFeedViewController ()

@property (nonatomic, readwrite, strong, nullable) NSTimer *dataTimer;
@property (nonatomic, readwrite, strong, nonnull) CPTMutableNumberArray *plotData;
@property (nonatomic, readwrite, strong, nonnull) CPTMutableNumberArray *plotDataForAnger;
@property (nonatomic, readwrite, strong, nonnull) CPTMutableNumberArray *plotDataForAverage;
@property (nonatomic, readwrite, assign) NSUInteger currentIndex;
@property (nonatomic, readwrite, assign) NSUInteger currentIndexForAnger;
@property (nonatomic, readwrite, assign) NSUInteger currentIndexForAverage;

@end

@implementation CameraFeedViewController

CPTGraph *graph;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.plotData  = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
    self.plotDataForAnger  = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
    self.plotDataForAverage  = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
    
    //[self addBarButton];
    [self setupGraph];
}

- (void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    [self createDetector]; // create the dector just before the view appears
}

- (void)viewWillDisappear:(BOOL)animated;
{
    [super viewWillDisappear:animated];
    [self destroyDetector]; // destroy the detector before the view disappears
}


-(void)addBarButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Overview"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(rightBarButtonTapped:)];
    
    [self.navigationItem setRightBarButtonItem:item];
    
}


#pragma mark - Action methods

-(void)rightBarButtonTapped : (id)sender{
    
}

#pragma mark - Graph Views

-(CGFloat)titleSize
{
    CGFloat size;
    
#if TARGET_OS_TV
    size = 36.0;
#elif TARGET_OS_SIMULATOR || TARGET_OS_IPHONE
    switch ( UI_USER_INTERFACE_IDIOM() ) {
        case UIUserInterfaceIdiomPad:
            size = 24.0;
            break;
            
        case UIUserInterfaceIdiomPhone:
            size = 2.0;
            break;
            
        default:
            size = 12.0;
            break;
    }
#else
    size = 24.0;
#endif
    
    return size;
}

-(void)setupGraph{
    
     CGRect bounds = self.hostingView.bounds;
    
    graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    self.hostingView.hostedGraph     = graph;
    self.hostingView.collapsesLayers = YES;
    [self.graphs addObject:graph];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    
    graph.plotAreaFrame.paddingTop    = self.titleSize * CPTFloat(0.5);
    graph.paddingRight  = 0.0;
    graph.plotAreaFrame.paddingBottom = 2.0;
    graph.plotAreaFrame.paddingLeft   = self.titleSize * CPTFloat(2.5);
    graph.plotAreaFrame.masksToBorder = NO;
    graph.paddingBottom = 1.0f;
    
    // Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.75;
    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.2)] colorWithAlphaComponent:CPTFloat(0.75)];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 0.25;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.1)];

    
    // X axis
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.labelingPolicy        = CPTAxisLabelingPolicyNone;
    x.orthogonalPosition    = @0.0;
    x.majorGridLineStyle    = majorGridLineStyle;
    x.minorGridLineStyle    = minorGridLineStyle;
    x.minorTicksPerInterval = 9;
    x.labelOffset           = x.labelOffset;
    //x.title                 = @"X Axis";
    x.titleOffset           = x.titleOffset;
    
    NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
    labelFormatter.numberStyle = NSNumberFormatterNoStyle;
    x.labelFormatter           = labelFormatter;
    
    // Y axis
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.orthogonalPosition    = @0.0;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    y.minorTicksPerInterval = 3;
    //y.labelOffset           = y.labelOffset * CPTFloat(0.25);
    //y.title                 = @"Y Axis";
    //y.titleOffset           = y.titleOffset * CPTFloat(1.25);
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    // Rotate the labels by 45 degrees, just to show it can be done.
    //x.labelRotation = CPTFloat(M_PI_4);
    
    
    [self addPlotForSmile];
    
    [self addPlotForAnger];
    
    [self addPlotForAverage];
    
    // Plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@(kMaxDataPoints - 2)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@1.0];
    plotSpace.allowsUserInteraction = YES;
    plotSpace.delegate = self;
    
    graph.legend                 = [CPTLegend legendWithGraph:graph];
    graph.legend.textStyle       = x.titleTextStyle;
    graph.legend.numberOfRows    = 1;
    graph.legend.fill            = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.legend.cornerRadius    = 5.0;
    graph.legendAnchor           = CPTRectAnchorTop;
    graph.legendDisplacement     = CGPointMake( 0.0, self.titleSize * CPTFloat(2.0));
    //graph.legend.frame = CGRectMake(graph.legend.frame.origin.x, graph.legend.frame.origin.y, graph.legend.frame.size.width, graph.legend.frame.size.height - 4.0f);
    
    
}

-(void) addPlotForSmile{
    // Create the plot
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = kPlotIdentifier;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 3.0;
    lineStyle.lineColor              = [CPTColor greenColor];
    dataSourceLinePlot.dataLineStyle = lineStyle;
    
    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];
}

-(void) addPlotForAnger{
    
    // Create the plot
    CPTScatterPlot *dataSourceLinePlotForAnger = [[CPTScatterPlot alloc] init];
    dataSourceLinePlotForAnger.identifier     = kPlotIdentifierForAnger;
    dataSourceLinePlotForAnger.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineWidth              = 3.0;
    lineStyle.lineColor              = [CPTColor redColor];
    dataSourceLinePlotForAnger.dataLineStyle = lineStyle;
    
    dataSourceLinePlotForAnger.dataSource = self;
    [graph addPlot:dataSourceLinePlotForAnger];
    
}

-(void) addPlotForAverage{
    // Create the plot
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = kPlotIdentifierForAverage;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineWidth              = 3.0;
    lineStyle.lineColor              = [CPTColor cyanColor];
    dataSourceLinePlot.dataLineStyle = lineStyle;
    
    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CPTPlotSpaceDelegate methods

-(CGPoint)plotSpace:(CPTPlotSpace *)space willDisplaceBy:(CGPoint)displacement {
    return CGPointMake(displacement.x, 0);
}

#pragma mark -
#pragma mark plot data methods

-(void)newData : (float) value//:(nonnull NSTimer *)theTimer
{
    CPTGraph *theGraph = self.hostingView.hostedGraph;
    CPTPlot *thePlot   = [theGraph plotWithIdentifier:kPlotIdentifier];
    
    if ( thePlot ) {
        if ( self.plotData.count >= kMaxDataPoints ) {
            [self.plotData removeObjectAtIndex:0];
            [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
        }
        
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
        NSUInteger location       = (self.currentIndex >= kMaxDataPoints ? self.currentIndex - kMaxDataPoints + 2 : 0);
        
        CPTPlotRange *oldRange = [CPTPlotRange plotRangeWithLocation:@( (location > 0) ? (location - 1) : 0 )
                                                              length:@(kMaxDataPoints - 2)];
        CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:@(location)
                                                              length:@(kMaxDataPoints - 2)];
        
        [CPTAnimation animate:plotSpace
                     property:@"xRange"
                fromPlotRange:oldRange
                  toPlotRange:newRange
                     duration:CPTFloat(1.0 / kFrameRate)];
        
        self.currentIndex++;
        [self.plotData addObject: @(value/100)];
        [thePlot insertDataAtIndex:self.plotData.count - 1 numberOfRecords:1];
    }
}


-(void)newDataForAnger : (float) value
{
    CPTGraph *theGraph = self.hostingView.hostedGraph;
    CPTPlot *thePlot   = [theGraph plotWithIdentifier:kPlotIdentifierForAnger];
    
    if ( thePlot ) {
        if ( self.plotDataForAnger.count >= kMaxDataPoints ) {
            [self.plotDataForAnger removeObjectAtIndex:0];
            [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
        }
        
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
        NSUInteger location       = (self.currentIndexForAnger >= kMaxDataPoints ? self.currentIndexForAnger - kMaxDataPoints + 2 : 0);
        
        CPTPlotRange *oldRange = [CPTPlotRange plotRangeWithLocation:@( (location > 0) ? (location - 1) : 0 )
                                                              length:@(kMaxDataPoints - 2)];
        CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:@(location)
                                                              length:@(kMaxDataPoints - 2)];
        
        [CPTAnimation animate:plotSpace
                     property:@"xRange"
                fromPlotRange:oldRange
                  toPlotRange:newRange
                     duration:CPTFloat(1.0 / kFrameRate)];
        
        self.currentIndexForAnger++;
        [self.plotDataForAnger addObject: @(value/100)];
        [thePlot insertDataAtIndex:self.plotDataForAnger.count - 1 numberOfRecords:1];
    }
}

-(void)newDataForAverage : (float) value
{
    CPTGraph *theGraph = self.hostingView.hostedGraph;
    CPTPlot *thePlot   = [theGraph plotWithIdentifier:kPlotIdentifierForAverage];
    
    if ( thePlot ) {
        if ( self.plotDataForAverage.count >= kMaxDataPoints ) {
            [self.plotDataForAverage removeObjectAtIndex:0];
            [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
        }
        
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
        NSUInteger location       = (self.currentIndexForAverage >= kMaxDataPoints ? self.currentIndexForAverage - kMaxDataPoints + 2 : 0);
        
        CPTPlotRange *oldRange = [CPTPlotRange plotRangeWithLocation:@( (location > 0) ? (location - 1) : 0 )
                                                              length:@(kMaxDataPoints - 2)];
        CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:@(location)
                                                              length:@(kMaxDataPoints - 2)];
        
        [CPTAnimation animate:plotSpace
                     property:@"xRange"
                fromPlotRange:oldRange
                  toPlotRange:newRange
                     duration:CPTFloat(1.0 / kFrameRate)];
        
        self.currentIndexForAverage++;
        [self.plotDataForAverage addObject: @(value/100)];
        [thePlot insertDataAtIndex:self.plotDataForAverage.count - 1 numberOfRecords:1];
    }
}


#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(nonnull CPTPlot *)plot
{
    if ([(NSString *)plot.identifier isEqualToString:kPlotIdentifierForAnger]) {
        return self.plotDataForAnger.count;
    }
    return self.plotData.count;
}

-(nullable id)numberForPlot:(nonnull CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    
    switch ( fieldEnum ) {
        case CPTScatterPlotFieldX:
            if ([(NSString *)plot.identifier isEqualToString:kPlotIdentifierForAnger]) {
                num = @(index + self.currentIndexForAnger - self.plotDataForAnger.count);
            }else if ([(NSString *)plot.identifier isEqualToString:kPlotIdentifierForAverage]){
                num = @(index + self.currentIndexForAverage - self.plotDataForAverage.count);
            }else{
                num = @(index + self.currentIndex - self.plotData.count);
            }
            
            break;
            
        case CPTScatterPlotFieldY:
            if ([(NSString *)plot.identifier isEqualToString:kPlotIdentifierForAnger]) {
                num = self.plotDataForAnger[index];
            }else if ([(NSString *)plot.identifier isEqualToString:kPlotIdentifierForAverage]){
                num = self.plotDataForAverage[index];
            }else{
                num = self.plotData[index];
            }
            
            break;
            
        default:
            break;
    }
    
    return num;
}


#pragma mark - Create and destroy camera detector

- (void)destroyDetector;
{
    [self.detector stop];
}

- (void)createDetector;
{
    // ensure the detector has stopped
    [self destroyDetector];
    
    // iterate through the capture devices to find the front position camera
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == AVCaptureDevicePositionFront)
        {
            
            self.detector = [[AFDXDetector alloc] initWithDelegate:self
                                                usingCaptureDevice:device
                                                      maximumFaces:2];
            
            self.detector.maxProcessRate = 5 ;
            
            // turn on all classifiers (emotions, expressions, and emojis)
            [self.detector setDetectAllEmotions:YES];
            [self.detector setDetectAllExpressions:YES];
            [self.detector setDetectEmojis:YES];
            [self.detector setDetectAllAppearances:YES];
            
            
            // turn on gender and glasses
            self.detector.gender = TRUE;
            self.detector.glasses = TRUE;
            
            // start the detector and check for failure
            NSError *error = [self.detector start];
            
            if (nil != error)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Detector Error"
                                                                               message:[error localizedDescription]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:
                 ^{}
                 ];
                
                return;
            }
            
            break;
        }
    }
}

#pragma mark -
#pragma mark AFDXDetectorDelegate Methods

// This is the delegate method of the AFDXDetectorDelegate protocol. This method gets called for:
// - Every frame coming in from the camera. In this case, faces is nil
// - Every PROCESSED frame that the detector
- (void)detector:(AFDXDetector *)detector hasResults:(NSMutableDictionary *)faces forImage:(UIImage *)image atTime:(NSTimeInterval)time;
{
    if (nil == faces)
    {
        [self unprocessedImageReady:detector image:image atTime:time];
        
    }
    else
    {
        [self processedImageReady:detector image:image faces:faces atTime:time];
    }
}

#pragma mark -
#pragma mark Convenience Methods

// This is a convenience method that is called by the detector:hasResults:forImage:atTime: delegate method below.
// You will want to do something with the face (or faces) found.
- (void)processedImageReady:(AFDXDetector *)detector image:(UIImage *)image faces:(NSDictionary *)faces atTime:(NSTimeInterval)time;
{
    // iterate on the values of the faces dictionary
    for (AFDXFace *face in [faces allValues])
    {
        NSLog(@"joyValue %f",face.emotions.surprise);
        NSLog(@"smile %f",face.expressions.smile);
        NSLog(@"anger %f",face.emotions.disgust);
        NSLog(@"sadness %f",face.emotions.sadness);
        
        NSLog(@"adasdasd");
        
        [self newData:face.expressions.smile];
        [self newDataForAnger:face.emotions.anger];
        [self newDataForAverage:face.emotions.surprise];
    }
}

// This is a convenience method that is called by the detector:hasResults:forImage:atTime: delegate method below.
// It handles all UNPROCESSED images from the detector. Here I am displaying those images on the camera view.
- (void)unprocessedImageReady:(AFDXDetector *)detector image:(UIImage *)image atTime:(NSTimeInterval)time;
{
    CameraFeedViewController * __weak weakSelf = self;
    
    // UI work must be done on the main thread, so dispatch it there.
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.cameraView setImage:image];
    });
}
@end
