//
//  CameraFeedViewController.h
//  EmotionGraph
//
//  Created by Doddabela, Supreeth A (US - Bengaluru) on 2/8/17.
//  Copyright © 2017 Deloitte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Affdex/Affdex.h>
#import "CorePlot-CocoaTouch.h"

@interface CameraFeedViewController : UIViewController<AFDXDetectorDelegate,CPTPlotDataSource,CPTPlotSpaceDelegate>

@property (strong) AFDXDetector *detector;
@property (strong) IBOutlet UIImageView *cameraView;
@property (strong,nonatomic)IBOutlet CPTGraphHostingView *hostingView;
@property (nonatomic, readwrite, strong, nonnull) NSMutableArray<__kindof CPTGraph *> *graphs;

@end
